<?php

use App\Http\Controllers\GaleriController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\KontakController;
use App\Http\Controllers\TentangController;
use App\Http\Controllers\UtamaController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', [HomeController::class, 'Hhome']);

Route::get('home', [HomeController::class, 'Hhome']);

Route::get('/tentang', [TentangController::class, 'Tentang']);

Route::get('/utama', [UtamaController::class, 'Utama']);

Route::get('/kontak', [KontakController::class, 'Kontak']);

Route::get('/galeri', [GaleriController::class, 'Galeri']);

