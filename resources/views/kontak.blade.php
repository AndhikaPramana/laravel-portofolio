 @extends('utama')
 @section('content')
     
 <!-- Contact -->
 <section id="kontak">
    <div class="container">
      <div class="row text-center mb-3">
        <div class="col">
          <h2>Kontak Saya</h2>
        </div>
      </div>
      <div class="row justify-content-center">
        <div class="col-md-6">
        <form>
            <div class="mb-3">
              <label for="nama" class="form-label">Nama</label>
              <input type="text" class="form-control" id="nama" aria-describedby="nama" placeholder="Nama Lengkap">
            </div>
            <div class="mb-3">
              <label for="email" class="form-label">Alamat Email</label>
              <input type="email" class="form-control" id="email" aria-describedby="email" placeholder="contoh@gmail.com">
            </div>
            <div class="mb-3">
            <label for="pesan" class="form-label">Pesan</label>
            <textarea class="form-control" id="pesan" rows="3"></textarea>
            </div>
            <button type="submit" class="btn btn-primary">Kirim</button>
        </form>
        </div>
      </div>

      <div class="icon1">
        <div class="row">
          <div class="col" >
            <a class="navbar-brand" aria-current="page"href="https://www.instagram.com/andhikaprmn__/" style="color:#6932e9;">
              <p><i class="bi bi-instagram" style="margin-right: 1rem"></i>@andhikaprmn__</p>
            </a>
          </div>
          <div class="col">
            <a class="navbar-brand" aria-current="page"href="https://web.facebook.com/andhika.pramana.3958/" style="color:blue;">
              <p><i class="bi bi-facebook" style="margin-right: 1rem"></i></i>Andhika</p>
            </a>
          </div>
          <div class="col">
            <a class="navbar-brand" aria-current="page" style="color:rgb(6, 177, 6);">
              <p><i class="bi bi-whatsapp" style="margin-right: 1rem"></i></i>081333085072</p>
            </a>
          </div>
          <div class="col">
            <a class="navbar-brand" aria-current="page"href="https://twitter.com/andhika_prmn" style="color:rgb(4, 241, 241);">
              <p><i class="bi bi-twitter" style="margin-right: 1rem"></i></i>Andhika Pramana Putra</p>
            </a>
          </div>
        </div>
      </div>
    </div>
  </section>

@endsection
<!-- Akhir contact -->