<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">

    {{-- AOS --}}
    <link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

    <!-- My CSS -->
      <link rel="stylesheet" href="CSS/home.css"/>
    <!-- Akhir CSS -->

    <title>Portofolio | Home</title>
  </head>
  <body>
    <audio autoplay>
      <source src="audio/audio2.mp3" type="audio/mpeg">
    </audio>
    <!-- Jumbotron -->
    <section class="jumbotron text-center" data-aos="fade-down"
    data-aos-easing="linear"
    data-aos-duration="1500">
        <img src="img/profile 1.jpeg" alt="I Made Andhika Pramana Putra" width="150" 
        class="rounded-circle img-thumbnail" />
        <h1 class="display-4">Selamat Datang di Portofolio</h1>
          <p>I Made Andhika Pramana Putra</p>
          <a class="btn btn-primary" href="/tentang" role="button">More</a>
      </section>
    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/js/bootstrap.bundle.min.js" integrity="sha384-/bQdsTh/da6pkI1MST/rWKFNjaCP5gBSY4sEBT38Q/9RBh9AH40zEOg7Hlq2THRZ" crossorigin="anonymous"></script>

    <script src="https://unpkg.com/aos@next/dist/aos.js"></script>
    <script>
      AOS.init();
    </script>
  </body>
</html>
