@extends('utama')

@section('content')
<!-- Galery -->
 <section id="galeri">
    <div class="container bg-color:#FDD752;"> 
      <div class="row text-center mb-3 ">
        <div class="col">
          <h2>Galeri</h2>
        </div>
      </div>
      <div class="row" data-aos="fade-up"
      data-aos-duration="3000">
        <div class="col-md-4 mb-3">
          <div class="card">
              <img src="img/galeri2.jpeg" class="card-img-top" alt="galeri1">
            <div class="card-body">
               <p class="card-text">Turnamen futsal antar club futsal se-Jembrana pada tahun 2018</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 mb-3">
          <div class="card">
              <img src="img/galeri3.jpeg" class="card-img-top" alt="galeri3">
            <div class="card-body">
               <p class="card-text">Kegiatan camping bersama teman-teman sekelas pada tahun 2019 akhir</p>
            </div>
          </div>
        </div>
        <div class="col-md-4 mb-3">
          <div class="card">
              <img src="img/galeri4.jpeg" class="card-img-top" alt="galeri4">
            <div class="card-body">
               <p class="card-text">Sebuah usaha dalam bidang desain, desain tersebut bernama Pets Portrait</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
<!-- Akhir galery -->