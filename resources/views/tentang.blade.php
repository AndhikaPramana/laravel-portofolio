@extends('utama')

@section('content')
<!-- About -->
<section id="tentang">
    <div class="container">
      <div class="row text-center mb-4">
        <div class="col-md">
          {{-- <h2>Tentang Saya</h2> --}}
          @include('jumbotron')
        </div>
      </div>
      <div class="row justify-content-center fs-5 text-center" >
        <div class="col">
          <p>{{ $title }}</p>
        </div> 
      </div>
    </div>
</section>
@endsection
<!-- Akhir about -->