<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class TentangController extends Controller
{
    public function Tentang(){
        return view('tentang',[
            'title'=>'Lahir pada tanggal 13 maret tahun 2000, saya berasal dari Kabupaaten Jembrana, Kota Negara, Kelurahan Baler Baler Agung, Lingkungan Kebon. Saat ini berusia 21 tahun dan sedang menempuh pendidikan di Universitas Pendidikan Ganesha. Kuliah mulai dari tahun 2019-sekarang. Saya alumni dari SDN 3 Baler Bale Agung, SMPN 2 Negara, dan SMKN 1 Negara. Hobi bermain sepak bola, bulutangkis, futsal, dan menonton film. Keseharian saya pada masa pandemi Covid-19 ini tentunya kuliah seperti biasa, namun dibarengi dengan bekerja sebagai admin bagi salah satu instansi Freelance untuk membantu ekonomi keluarga pada masa pandemi seperti ini.']);
    }
}
